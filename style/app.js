$(document).ready(function() {
    var altura = $('nav').offset().top;
    $(window).on('scroll', function() {
        if($(window).scrollTop() > 10) {
            $('nav').addClass('shrink');
        }else {
            $('nav').removeClass('shrink');
            $('nav').addClass('top');
        }
    });




    $('#home').on('click',function(){
        var position = 0;
        $('body, html').animate({
            scrollTop: position + 'px'
        } , 1000);
        $('#home').addClass('bottom'); 
        $('#sistem').removeClass('bottom');
        $('#about').removeClass('bottom');
        $('#contact').removeClass('bottom');
    })

    $('#sistem').on('click',function(){
        var position = 450;
        $('body, html').animate({
            scrollTop: position + 'px'
        } , 1000);
        $('#sistem').addClass('bottom'); 
        $('#home').removeClass('bottom');
        $('#about').removeClass('bottom');
        $('#contact').removeClass('bottom');
    })

    $('#about').on('click',function(){
        var position = 750;
        $('body, html').animate({
            scrollTop: position + 'px'
        } , 1000);
        $('#aobut').addClass('bottom'); 
        $('#sistem').removeClass('bottom');
        $('#home').removeClass('bottom');
        $('#contact').removeClass('bottom');
    })

    $('#contact').on('click',function(){
        var position = $('#form').offset().top;
        $('body, html').animate({
            scrollTop: position + 'px'
        } , 1000);
        $('#contact').addClass('bottom'); 
        $('#sistem').removeClass('bottom');
        $('#about').removeClass('bottom');
        $('#home').removeClass('bottom');
    })
    
});